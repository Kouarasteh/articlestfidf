import math
import numpy as np
import sys
import csv
import hashlib
from sklearn.datasets import fetch_20newsgroups

maxInt = sys.maxsize
decrement = True

while decrement:
    # decrease the maxInt value by factor 10
    # as long as the OverflowError occurs.

    decrement = False
    try:
        csv.field_size_limit(maxInt)
    except OverflowError:
        maxInt = int(maxInt/10)
        decrement = True

# TF-IDF for keywords
def tf(word,blob):
 return ((blob.split().count(word)) / (len(blob.split())))
def n_containing(word, bloblist):
    n = 0
    for blob in bloblist:
         if word in blob.split():
             n+=1
    return n
def idf(word, bloblist):
    return math.log(len(bloblist) / (1 + n_containing(word, bloblist)))

def tfidf(word, blob, bloblist):
    return tf(word, blob)*idf(word,bloblist)

#importing Article Data
with open('articles1.csv', newline='',encoding="utf8") as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    articles = []
    for row in spamreader:
        articles.append(row)
bloblist = []
for idx in range(1,20):
    bloblist.append(articles[idx][9])

#create dictionary for mapping from article title to top 10 keywords
a2k = {}
for i, blob in enumerate(bloblist):
    scores = {word: tfidf(word, blob, bloblist) for word in bloblist[i].split()}
    sorted_words = sorted(scores.items(), key=lambda x: x[1], reverse=True)
    a2k[articles[i+1][2]] = sorted_words[0:10]
    #a2k[articles[i][0]] = sortedwords
    print(articles[i+1][2])
    print(sorted_words[0:10])
