#ArticlesTFIDF

This code finds keywords in an articles using the Term Frequency - Inverse Document Frequency algorithm. 

It then maintains a list of the top ten unique keywords per document to recommend topics.